The www.LiftingResearch.com Codebase
==============================================================================

# Introduction

The site www.LiftingResearch.com is a WordPress website that provides:
  * consolidated, easy-to-read content about weightlifting topics
  * tools, like 1-rep max estimators

# Development Strategy

WordPress is a Content Management System that is itself evolving, along with
its plugins from the extensive ecosystem, and what files to version is an
important question for the project. The current decision is to version:
  * The *liftingresearch theme* folder in wp-content/themes
  * The *plugins* folder, in wp-content, 
    just to eliminate the startup step of having to
    download the plugins in the Admin console for every new development
    environment
  * The *.htaccess* file in wp-content/uploads. This applies the technique from
    https://stevegrunwell.com/blog/keeping-wordpress-under-version-control-with-git/
    which redirects requests for media to the live website. This lets us get
    around versioning hundreds of images that do not really belong in version
    control.
  * The *.gitignore* file, which has a lot in it! We are relying on the WordPress
    install for these files

While dealing with a variety of potentially changing files from the WordPress
ecosystem adds some instability. The upside is that the site's content and
development can be completely independent streams of work. The look of the
admin console won't change much, so a writer for the site has a polished writing
environment on day 1, even if the site isn't itself polished for 6 months.

# Local Development

## XAMPP for Web Server and DataBase
WordPress needs a Webserver to handle server side requests (typically PHP) and
a database to store the content of the site (e.g., blog article text). The tool
[XAMPP](https://sourceforge.net/projects/xampp/) is an excellent tool for
developing a wordpress site locally. After downloading XAMPP, run the program
and click start on just the Apach and MySQL Modules. You're ready to install
WordPress.

## Installing WordPress
### The Web Server
First, to test out XAMPP with a regular website, find the location where XAMPP
is installed (perhaps C:\xampp) and create a new folder in the htdocs
directory, perhaps called `test`. In that folder, create a file called
`index.php` and enter content similar to the following:
```
<html>
<head>
</head>

<body>
<p>
Hello, 2 + 2 is <?php echo 2 + 2; ?>
</p>

</body>
</html>
```
With XAMPP's Apache Module running, when you go to localhost/test in your
browser, you should see the result of the calculation displayed.

To get practice with PHP, it's helpful to run it in an interactive
environment. Click the "Shell" button in the XAMPP console and then
type `php -a`. Try the command `echo 2 + 2;` Type `exit` to leave. If you
prefer to stay in Ubuntu, then first install PHP with 
`sudo apt-get install php`.
