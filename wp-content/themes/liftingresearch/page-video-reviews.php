<?php

/** Template Name: Video Reviews

*/
?>

<?php get_header();?>
<div class="blog-wrapper col-2-items">

    <?php
        $args = array(
          'post_type' => 'video_review'
        );
      $the_query = new WP_Query($args);
    ?>

    <?php
        if ($the_query->have_posts()):
            while ($the_query->have_posts()):
                $the_query->the_post();
    ?>
        <div class="single-post col-2-item">
            <h1 class="post-title"><a href="<?php the_permalink();?>">
                <?php the_title(); ?> </a></h1>
                <div class="featured-image">
                  <?php the_post_thumbnail('blog-thumbnail'); ?>
                  <span class="image-overlay"></span>
                </div>
            <div class="video-review-meta">
                <?php
                    $dor = get_field('date_of_release', false, false);
                    $dor = new DateTime($dor);
                ?>
                <em>Lifting Research Posted on <?php echo
                     get_the_date(); ?></em>
                <br/>
                <em>Fitness Professional - <?php
                    the_field('fitness_professional'); ?></em>
                <br/>
                <em>Video's Date of Release - <?php
                     echo $dor->format('j M Y'); ?></em>

            </div>
            <?php the_excerpt();?>
        </div>
        <?php endwhile; ?>

    <?php endif;?>

</div>
<?php dynamic_sidebar('video-review-sidebar'); ?>
<?php get_footer()?>
