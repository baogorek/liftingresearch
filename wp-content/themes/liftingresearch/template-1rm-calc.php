<?php

/**
 * Template Name: Estimate 1-RM
 *
 */

?>
<?php
  get_header();
?>

<h1><?php the_title() ?></h1>

<p>
This calculator implements functions from "<em>The Accuracy of Prediction
Equations for Estimating 1-RM Performance in the Bench Press, Squat, and
Deadlift</em>" by LeSuer et al (1997) in the <em>Journal of Strength and
Conditioning Research.</em>
</p>

<h2> Enter N-Rep Max information below </h2>

<form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>"
    method="post">
    <label for="reps">Number of Reps: </label>
    <input type="number" min="0" max="4000" name="reps" id="reps" required>

    <label for="rep_wt">Rep Weight in lbs: </label>
    <input type="number" min="0" max="4000" name="rep_wt" id="rep_wt" required>
    <br>
    <!-- Defining my custom action below -->
    <input type="hidden" name="action" value="my_action_for_1rm_calc"><br>
    <button type="submit" value="Estimate 1-RM" class="button">Estimate 1-RM! </button>
</form>

<img src="<?php echo content_url(); ?>/uploads/2019/06/table_of_1rm_calcs.png"
     alt="One Rep Max equations">


<h2 id="pagecalcs"> One Rep Max Calculations </h2>

<h3>Brzycki 1-RM Estimate</h3>
<?php echo get_query_var( 'brzycki-1rm', 'NA' ); ?> lbs.

<h3> Epley 1-RM Estimate</h3>
<?php echo get_query_var( 'epley-1rm', 'NA' ); ?> lbs.

<h3> Lander 1-RM Estimate</h3>
<?php echo get_query_var( 'lander-1rm', 'NA' ); ?> lbs.

<h3> Lombardi 1-RM Estimate </h3>
<?php echo get_query_var( 'lombardi-1rm', 'NA' ); ?> lbs.

<h3> Mayhew et al. 1-RM Estimate </h3>
<?php echo get_query_var( 'mayhew-1rm', 'NA' ); ?> lbs.

<h3> O'conner et al. 1-RM Estimate </h3>
<?php echo get_query_var( 'oconner-1rm', 'NA' ); ?> lbs.

<h3> Wathan 1-RM Estimate </h3>
<?php echo get_query_var( 'wathan-1rm', 'NA' ); ?> lbs.

<?php
  get_footer();
?>
