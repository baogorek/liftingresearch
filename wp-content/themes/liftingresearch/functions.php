<?php
/**
 * liftingresearch functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package liftingresearch
 */

if ( ! function_exists( 'liftingresearch_setup' ) ) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function liftingresearch_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on liftingresearch, use a find and replace
     * to change 'liftingresearch' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'liftingresearch', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    // This theme uses wp_nav_menu() in one location.
    register_nav_menus( array(
      'menu-1' => esc_html__( 'Primary', 'liftingresearch' ),
    ) );

    register_nav_menus( array(
      'header' => esc_html__( 'Header', 'liftingresearch' ),
    ) );

    /*
     * Switch default core markup for search form, comment form, and comments
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
    ) );

    // Set up the WordPress core custom background feature.
    add_theme_support( 'custom-background', apply_filters( 'liftingresearch_custom_background_args', array(
      'default-color' => 'ffffff',
      'default-image' => '',
    ) ) );

    // Add theme support for selective refresh for widgets.
    add_theme_support( 'customize-selective-refresh-widgets' );

    /**
     * Add support for core custom logo.
     *
     * @link https://codex.wordpress.org/Theme_Logo
     */
    add_theme_support( 'custom-logo', array(
      'height'      => 250,
      'width'       => 250,
      'flex-width'  => true,
      'flex-height' => true,
    ) );
  }
endif;
add_action( 'after_setup_theme', 'liftingresearch_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function liftingresearch_content_width() {
  // This variable is intended to be overruled from themes.
  // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
  // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
  $GLOBALS['content_width'] = apply_filters( 'liftingresearch_content_width', 640 );
}
add_action( 'after_setup_theme', 'liftingresearch_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function liftingresearch_widgets_init() {
  register_sidebar( array(
    'name'          => esc_html__('Blog', 'liftingresearch'),
    'id'            => 'blog-sidebar',
    'description'   => esc_html__( 'Sidebar for the blog page.',
                                  'liftingresearch' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__('Footer', 'liftingresearch'),
    'id'            => 'footer-sidebar',
    'description'   => esc_html__( 'Sidebar for the footer.',
                                  'liftingresearch' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Video Reviews', 'liftingresearch' ),
    'id'            => 'video-review-sidebar',
    'description'   => esc_html__( 'Sidebar for video reviews.',
                                   'liftingresearch' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

  register_sidebar( array(
    'name'          => esc_html__( 'Program Reviews', 'liftingresearch' ),
    'id'            => 'program-review-sidebar',
    'description'   => esc_html__( 'Add widgets here.', 'liftingresearch' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h3 class="widget-title">',
    'after_title'   => '</h3>',
  ) );

}
add_action( 'widgets_init', 'liftingresearch_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function liftingresearch_scripts() {
  wp_enqueue_style( 'liftingresearch-style', get_stylesheet_uri() );

  wp_enqueue_style( 'header',
                    get_template_directory_uri().'/css/header.css');

  wp_enqueue_style( 'styles',
                    get_template_directory_uri().'/css/styles.css');

  wp_enqueue_style( 'footer',
                    get_template_directory_uri().'/css/footer.css');

  wp_enqueue_script( 'liftingresearch-navigation',
                     get_template_directory_uri() . '/js/navigation.js',
                      array(), '20151215', true );

  wp_enqueue_script( 'liftingresearch-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

  if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
    wp_enqueue_script( 'comment-reply' );
  }
}
add_action( 'wp_enqueue_scripts', 'liftingresearch_scripts' );

/* Custom Image Sizes */

add_image_size('blog-thumbnail', 500, 250, true);
add_image_size('post-thumbnail', 1000, 600, true);

/* Customize Read More Link */
/**
 * Filter the "read more" excerpt string link to the post.
 *
 * @param string $more "Read more" excerpt string.
 * @return string (Maybe) modified "read more" excerpt string.
 */
function wpdocs_excerpt_more( $more ) {
    if ( ! is_single() ) {
        $more = sprintf( '<a class="read-more" href="%1$s">%2$s</a>',
            get_permalink( get_the_ID() ),
            __( 'Read More', 'textdomain' )
        );
    }
 
    return $more;
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );

/* Customize Excerpt Length */
function wpdocs_custom_excerpt_length( $length ) {
  return 35;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
  require get_template_directory() . '/inc/jetpack.php';
}

/* Custom Functions */

function removeVersion() {
    return '';
}
add_filter('the_generator', 'removeVersion');

function add_query_vars_filter( $vars ) {
  // Add custom query variables for use with get_query_var
  array_push($vars, "brzycki-1rm", "epley-1rm", "lander-1rm", "lombardi-1rm",
             "mayhew-1rm", "oconner-1rm", "wathan-1rm");
  
  return $vars;
}
add_filter( 'query_vars', 'add_query_vars_filter' );

function calculate_1rm() {
    
    $reps = $_POST["reps"];
    $rep_wt = $_POST["rep_wt"];

    $brzycki = 100. * $rep_wt / (102.78 - 2.78 * $reps);
    $epley = (1. + .0333 * $reps) * $rep_wt;
    $lander = 100. * $rep_wt / (101.3 - 2.67123 * $reps);
    $lombardi = $rep_wt * $reps ** 0.1;
    $mayhew = 100. * $rep_wt / (52.2 + 41.9 * exp(-.055 * $reps));
    $oconner = $rep_wt * (1 + .025 * $reps);
    $wathan = 100. * $rep_wt / (48.8 + 53.8 * exp(-.075 * $reps));

    wp_redirect(add_query_arg(array('brzycki-1rm' => round($brzycki, 1),
                                    'epley-1rm' => round($epley, 1),
                                    'lander-1rm' => round($lander, 1),
                                    'lombardi-1rm' => round($lombardi, 1),
                                    'mayhew-1rm' => round($mayhew, 1),
                                    'oconner-1rm' => round($oconner, 1),
                                    'wathan-1rm' => round($wathan, 1)
                                   ),
        'www.liftingresearch.com/one-rep-max-estimation#pagecalcs' ) );

    exit;
}
add_action( 'admin_post_nopriv_my_action_for_1rm_calc', 'calculate_1rm' );
add_action( 'admin_post_my_action_for_1rm_calc', 'calculate_1rm' );

