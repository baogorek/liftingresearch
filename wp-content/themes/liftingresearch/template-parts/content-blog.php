<div class="blog-wrapper col-2-items">
    <?php
        $args = array(
          'posts_per_page' => -1 
        );
    
      $the_query = new WP_Query($args);
    ?>
    
    <?php
        if ($the_query->have_posts()):
            while ($the_query->have_posts()):
                $the_query->the_post();
    ?>
        <div class="single-post"> 
            <h1 class="post-title"><a
               href="<?php the_permalink();?>">
                <?php the_title(); ?> </a></h1>
                <div class="featured-image"> 
                  <?php the_post_thumbnail('blog-thumbnail'); ?>
                  <span class="image-overlay"></span>
                </div>
            <div class="post-meta">
                <em>Posted on <?php echo get_the_date(); ?></em>
                <br/>
                <em>Written by <?php the_author(); ?></em>
            </div>
            <?php the_excerpt();?>
        </div>
        <?php endwhile; ?>
    <?php endif; ?>
</div>
