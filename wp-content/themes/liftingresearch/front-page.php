<?php get_header();?>
    <div id="primary" class="content-area">
    <main id="main" class="site-main">

        <!-- Query for video reviews -->
        <?php wp_reset_postdata(); ?>
        <div class="row-2">
            <?php
                $args = array(
                  'post_type' => 'video_review',
                  'posts_per_page' => 2 
                );

              $the_query = new WP_Query($args);
            ?>
            <h2 class="row-title">Recent Video Reviews:</h2>
            <hr>
            <?php
                if ($the_query->have_posts()):
                    while ($the_query->have_posts()):
                        $the_query->the_post();
            ?>
                <div class="single-post">
                    <h1 class="post-title"><a href="<?php the_permalink();?>">
                        <?php the_title(); ?> </a></h1>
                        <div class="featured-image">
                          <?php the_post_thumbnail('blog-thumbnail'); ?>
                          <span class="image-overlay"></span>
                        </div>
                    <div class="post-meta">
                        <em>Posted on <?php echo get_the_date(); ?></em>
                        <br/>
                        <em>Written by <?php the_author(); ?></em>
                    </div>
                    <?php the_excerpt();?>
                </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>

      <!-- Query for program reviews -->
        <?php wp_reset_postdata(); ?>
        <div class="row-3">
            <?php
                $args = array(
                  'post_type' => 'program_review',
                  'posts_per_page' => 2 
                );

              $the_query = new WP_Query($args);
            ?>
            <h2 class="row-title">Recent Program Reviews:</h2>
            <hr>
            <?php
                if ($the_query->have_posts()):
                    while ($the_query->have_posts()):
                        $the_query->the_post();
            ?>
                <div class="single-post">
                    <h1 class="post-title"><a href="<?php the_permalink();?>">
                        <?php the_title(); ?> </a></h1>
                        <div class="featured-image">
                          <?php the_post_thumbnail('blog-thumbnail'); ?>
                          <span class="image-overlay"></span>
                        </div>
                    <div class="post-meta">
                        <em>Posted on <?php echo get_the_date(); ?></em>
                        <br/>
                        <em>Written by <?php the_author(); ?></em>
                    </div>
                    <?php the_excerpt();?>
                </div>
            <?php endwhile; ?>
            <?php endif; ?>
        </div>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_footer();
?>
