<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package liftingresearch
 */

?>
    </div><!-- .container -->
  </div><!-- #content -->

  <footer id="colophon" class="site-footer">
    <div class="container">
      <div class="footer-sidebar"> <!-- # What gives it the sleek black look -->
        <?php dynamic_sidebar("Footer") ?>
      </div>
      <div class="site-info">
        Copyright 2019 LiftingResearch.com
      </div><!-- .site-info -->
    </div><!--.container -->
  </footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
