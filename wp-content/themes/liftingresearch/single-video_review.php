<?php
/**
* Template for displaying single video reviews.
*/
?>

<?php get_header();?>

  <div id="primary" class="content-area">
      <main id="main" class="site-main">

      <?php
      while (have_posts()) : the_post(); ?>
          <h2><?php the_title(); ?> </h2>
          <h3>Fitness Professional - <?php the_field('fitness_professional');
              ?></h3>
          <?php the_post_thumbnail(); ?>
          <h3>The Review</h3>
          <?php the_content(); ?>

      <?php endwhile; // End of the loop.
      ?>

      </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
?>
