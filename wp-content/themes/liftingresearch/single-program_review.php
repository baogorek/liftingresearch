<?php
/**
* Template for displaying single program reviews.
*/
?>

<?php get_header();?>

  <div id="primary" class="content-area">
      <main id="main" class="site-main">

      <?php
      while (have_posts()) : the_post(); ?>
          <h1><?php the_title(); ?> </h1>
          <?php the_post_thumbnail(); ?>
          <?php the_content(); ?>

      <?php endwhile; // End of the loop.
      ?>

      </main><!-- #main -->
  </div><!-- #primary -->

<?php
get_sidebar();
get_footer();
?>

