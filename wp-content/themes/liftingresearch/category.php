<?php
/**
 * The category template file
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package liftingresearch
 */

get_header();
?>
  <div id="primary" class="content-area">
    <main id="main" class="site-main">
    <h1> <?php single_cat_title('Category - '); ?> </h1>
    <?php
    if ( have_posts() ) :

      if ( is_home() && ! is_front_page() ) :
        ?>
        <header>
          <h1 class="page-title screen-reader-text"><?php
            single_post_title(); ?></h1>
        </header>
        <?php
      endif;

      /* Start the Loop - code has been deleted from lecture 40: */

        /*
         * Include the Post-Type-specific template for the content.
         * If you want to override this in a child theme, then include a file
         * called content-___.php (where ___ is the Post Type name) and that
           will be used instead.
         */
        get_template_part( 'template-parts/content', 'blog' );

    else :

      get_template_part( 'template-parts/content', 'none' );

    endif;
    ?>

    </main><!-- #main -->
  </div><!-- #primary -->

<?php
dynamic_sidebar('blog-sidebar');
get_footer();
