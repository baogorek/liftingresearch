<?php

/** Template Name: Program Reviews


*/
?>

<?php get_header();?>

<div class="blog-wrapper col-2-items">

    <?php
        $args = array(
          'post_type' => 'program_review'
        );
      $the_query = new WP_Query($args);
    ?>

    <?php
        if ($the_query->have_posts()):
            while ($the_query->have_posts()):
                $the_query->the_post();
    ?>
        <div class="single-post col-2-item">
            <h1 class="post-title"><a href="<?php the_permalink();?>">
                <?php the_title(); ?> </a></h1>
                <div class="featured-image">
                  <?php the_post_thumbnail('blog-thumbnail'); ?>
                  <span class="image-overlay"></span>
                </div>
            <div class="program-review-meta">
                <em>Lifting Research Posted on <?php echo
                     get_the_date(); ?></em>
                <br/> <br/>
                <em> Cost - <?php
                    the_field('cost'); ?></em><br/>
                <em>Length of Program - <?php
                    the_field('length_of_program'); ?></em><br/>
                <em>Technical Difficulty - <?php
                    the_field('technical_difficulty'); ?></em><br/>
                <em>Attention to Detail - <?php
                    the_field('attention_to_detail'); ?></em><br/>

                <?php the_excerpt();?>
            </div>
        </div>
        <?php endwhile; ?>

    <?php endif;?>

</div>
<?php dynamic_sidebar('program-review-sidebar'); ?>
<?php get_footer(); ?>

